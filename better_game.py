from random import randint

guess = 1
month = randint(1,12)
year = randint(1924,2004)

name = input("Hi! What is your name? ")

for tries in range(0,5):
    month = randint(1,12)
    year = randint(1924,2004)
    print(f"Guess {guess}: {name} were you born in {month} / {year} ?")
    answer = input("Yes or no? ").lower()

    if answer =="no" and guess == 5:
        print("I have other things to do. Good bye.")
        exit()
    elif answer == "no" and guess < 5:
        print("Let me try again!")
        guess += 1
    elif answer == "yes":
        print("I knew it!")
        exit()
    else:
        print("Invalid response")
        exit()
